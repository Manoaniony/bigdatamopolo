vagrant up 
vagrant ssh

-- Démarrage de Hadoop (HDFS et YARN)
start-dfs.sh
start-yarn.sh

-- Démarrage du serveur Hadoop HIVE
nohup hive --service metastore > /dev/null &
nohup hiveserver2 > /dev/null &

nohup java -Xmx64m -Xms64m -jar $KVHOME/lib/kvstore.jar kvlite -secure-config disable -root $KVROOT &


cd /vagrant

javac -g -cp $KVHOME/lib/kvclient.jar:vagrant/mbds mbds/Tpa.java

java -Xmx256m -Xms256m  -cp $KVHOME/lib/kvclient.jar:/vagrant/ mbds.Tpa

beeline
!connect jdbc:hive2://localhost:10000
	-> oracle
	-> oracle

-- Dans un autre cmd 
vagrant ssh

cd /vagrant/analyse

jupyter notebook --ip=0.0.0.0