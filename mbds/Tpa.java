package mbds;

import oracle.kv.KVStore;
import java.util.List;
import java.util.Iterator;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;
import oracle.kv.FaultException;
import oracle.kv.StatementResult;
import oracle.kv.table.TableAPI;
import oracle.kv.table.Table;
import oracle.kv.table.Row;
import oracle.kv.table.PrimaryKey;
import oracle.kv.ConsistencyException;
import oracle.kv.RequestTimeoutException;
import java.lang.Integer;
import oracle.kv.table.TableIterator;
import oracle.kv.table.EnumValue;
import java.io.File;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.List;

public class Tpa {

    private final KVStore store;
    private final String myTpPath="/vagrant/DataSourceTpa";
    private final String tabCatalogue="CATALOGUE";
    private final String tabMarketing="MARKETING";
    
    public static void main(String args[]) {
        try {
            Tpa tpa= new Tpa(args);
            tpa.initTpaTablesAndData(tpa);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    Tpa(String[] argv) {
        String storeName = "kvstore";
        String hostName = "localhost";
        String hostPort = "5000";

        final int nArgs = argv.length;
        int argc = 0;
        store = KVStoreFactory.getStore(new KVStoreConfig(storeName, hostName + ":" + hostPort));
    }

    private void displayResult(StatementResult result, String statement) {
        if (result.isSuccessful()) {
            System.out.println("   -> Statement was successful");
        } else if (result.isCancelled()) {
            System.out.println("   -> Statement was cancelled:\n\t" + statement);
        } else {
            if (result.isDone()) {
                System.out.println("   -> Statement failed:\n\t" + statement);
                System.out.println("   -> Problem:\n\t" + result.getErrorMessage());
            }
            else {
                System.out.println("   -> Statement in progress:\n\t" + statement);
            }
        }
    }

    public void initTpaTablesAndData(Tpa tpa) {
        tpa.dropTableCatalogue();
        tpa.dropTableMarketing();
        tpa.createTableCatalogue();
        tpa.createTableMarketing();
        tpa.loadCatalogueDataFromFile(myTpPath + "/Catalogue.csv");
        tpa.loadMarketingDataFromFile(myTpPath + "/Marketing.csv");
    }

    public void dropTableCatalogue() {
        System.out.println("[*]  DROP-TABLE-CATALOGUE");
        String statement = null;

        statement ="drop table " + tabCatalogue;
        executeDDL(statement);
    }

    public void dropTableMarketing() {
        System.out.println("[*]  DROP-TABLE-MARKETING");
        String statement = null;

        statement ="drop table " + tabMarketing;
        executeDDL(statement);
    }

    public void createTableCatalogue() {
        System.out.println("[*]  CREATE-TABLE-CATALOGUE");
        String statement = null;
        statement="Create table " + tabCatalogue + " ("
                + "CATALOGUEID INTEGER,"
                + "MARQUE STRING,"
                + "NOM STRING,"
                + "PUISSANCE INTEGER,"
                + "LONGUEUR STRING,"
                + "NB_PLACES INTEGER,"
                + "NB_PORTES INTEGER,"
                + "COULEUR STRING,"
                + "OCCASION BOOLEAN,"
                + "PRIX DOUBLE,"
                + "PRIMARY KEY (CATALOGUEID))";
        executeDDL(statement);
    }

    public void createTableMarketing() {
        System.out.println("[*]  DROP-TABLE-MARKETING");
        String statement = null;
        statement="create table " + tabMarketing + " ("
                + "MARKETINGID INTEGER,"
                + "AGE INTEGER,"
                + "SEXE STRING,"
                + "TAUX INTEGER,"
                + "SITUATION_FAMILIALE STRING,"
                + "NB_ENFANTS_ACHARGE  INTEGER,"
                + "DEUXIEME_VOITURE BOOLEAN,"
                + "PRIMARY KEY (MARKETINGID))";
        executeDDL(statement);
    }
    
    public void executeDDL(String statement) {
        TableAPI tableAPI = store.getTableAPI();
        StatementResult result = null;

        try {
            result = store.executeSync(statement);
            displayResult(result, statement);
        } catch (IllegalArgumentException e) {
            System.out.println(" -> Invalid statement:\n" + e.getMessage());
        } catch (FaultException e) {
            System.out.println(" -> Statement couldn't be executed, please retry: " + e);
        }
    }
    
    private void insertACatalogueRow(int catalogueId, String marque, String nom, int puissance,
                                     String longueur, int nbPlaces, int nbPortes, String couleur,
                                     boolean occasion, double prix){
        System.out.println("[*]  INSERT-CATALOGUE-ROW");
        StatementResult result = null;
        String statement = null;
        
        try {
            TableAPI tableH = store.getTableAPI();
            Table tableCatalogue = tableH.getTable(tabCatalogue);
            Row row = tableCatalogue.createRow();

            row.put("catalogueid", catalogueId);
            row.put("marque", marque);
            row.put("nom", nom);
            row.put("puissance", puissance);
            row.put("longueur", longueur);
            row.put("nb_places", nbPlaces);
            row.put("nb_portes", nbPortes);
            row.put("couleur", couleur);
            row.put("occasion", occasion);
            row.put("prix", prix);
            System.out.println(" -> " + row);
            tableH.put(row, null, null);
        }
        catch (IllegalArgumentException e) {
            System.out.println("Invalid statement:\n" + e.getMessage());
        }
        catch (FaultException e) {
            System.out.println("Statement couldn't be executed, please retry: " + e);
        }
    }
    
    private void insertAMarketingRow(int marketingId, int age, String sexe,
                                     int taux, String situationFamiliale,
                                     int nbEnfantsACharge, boolean deuxiemeVoiture){
        System.out.println("[*]  INSERT-MARKETING-ROW");
        StatementResult result = null;
        String statement = null;
        
        try {
            TableAPI tableH = store.getTableAPI();
            Table tableMarketing = tableH.getTable(tabMarketing);
            
            Row row = tableMarketing.createRow();
            row.put("marketingid", marketingId);
            row.put("age", age);
            row.put("sexe", sexe);
            row.put("taux", taux);
            row.put("situation_familiale", situationFamiliale);
            row.put("nb_enfants_acharge", nbEnfantsACharge);
            row.put("deuxieme_voiture", deuxiemeVoiture);
            System.out.println(" -> " + row);
            tableH.put(row, null, null);
        }
        catch (IllegalArgumentException e) {
            System.out.println("Invalid statement:\n" + e.getMessage());
        }
        catch (FaultException e) {
            System.out.println("Statement couldn't be executed, please retry: " + e);
        }
    }

    void loadCatalogueDataFromFile(String catalogueDataFileName){
        InputStreamReader ipsr;
        BufferedReader br = null;
        InputStream ips;
        
        String ligne;
        int compteur = 0;
        
        try {
            ips = new FileInputStream(catalogueDataFileName);
            ipsr = new InputStreamReader(ips);
            br = new BufferedReader(ipsr);
            
            while ((ligne = br.readLine()) != null) {
                if(compteur > 0){
                    ArrayList<String> catalogueRecord= new ArrayList<String>();
                    StringTokenizer val = new StringTokenizer(ligne,",");
                    while(val.hasMoreTokens()) {
                        catalogueRecord.add(val.nextToken().toString());
                    }

                    String marque = catalogueRecord.get(0);
                    String nom = catalogueRecord.get(1);
                    int puissance = Integer.parseInt(catalogueRecord.get(2));
                    String longueur	= catalogueRecord.get(3);
                    int nbPlaces = Integer.parseInt(catalogueRecord.get(4));
                    int nbPortes = Integer.parseInt(catalogueRecord.get(5));
                    String couleur = catalogueRecord.get(6);
                    boolean occasion = Boolean.parseBoolean(catalogueRecord.get(7));
                    double prix	= Double.parseDouble(catalogueRecord.get(8));

                    this.insertACatalogueRow(compteur, marque, nom, puissance, longueur, nbPlaces, nbPortes, couleur, occasion, prix);
                }
                compteur++;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    void loadMarketingDataFromFile(String marketingDataFileName){
        InputStreamReader ipsr;
        BufferedReader br = null;
        InputStream ips;
        String ligne;
        int compteur = 0;

        try {
            ips = new FileInputStream(marketingDataFileName);
            ipsr = new InputStreamReader(ips);
            br = new BufferedReader(ipsr);

            while ((ligne = br.readLine()) != null) {
                if(compteur > 0){
                    ArrayList<String> marketingRecord= new ArrayList<String>();
                    StringTokenizer val = new StringTokenizer(ligne,",");
                    while(val.hasMoreTokens()) {
                        marketingRecord.add(val.nextToken().toString());
                    }

                    int age = Integer.parseInt(marketingRecord.get(0));
                    String sexe	= marketingRecord.get(1);
                    int taux = Integer.parseInt(marketingRecord.get(2));
                    String situationFamiliale = marketingRecord.get(3);
                    int nbEnfantsACharge = Integer.parseInt(marketingRecord.get(4));
                    boolean deuxiemeVoiture = Boolean.parseBoolean(marketingRecord.get(5));

                    this.insertAMarketingRow(compteur, age, sexe, taux, situationFamiliale, nbEnfantsACharge, deuxiemeVoiture);
                }
                compteur++;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }

}
