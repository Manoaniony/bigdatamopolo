-- MAP REDUCE 
    javac CO2*.java
    mkdir -p org/mbds/tpa
    mv CO2*.class org/mbds/tpa
    jar -cvf co2.jar -C . org
    hadoop fs -rm -r /result
    hadoop jar co2.jar org.mbds.tpa.CO2Driver /data_source_tpa/co2/CO2.csv /result
    hadoop fs -mkdir /result/co2
    hadoop fs -mv /result/part-r-00000 /result/co2
