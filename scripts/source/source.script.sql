-- Démarrage de Hadoop (HDFS et YARN)
start-dfs.sh
start-yarn.sh

=============

-- Copie des fichiers source dans les répertoires appropriés sur HDFS
hadoop fs -mkdir /data_source_tpa
hadoop fs -mkdir /data_source_tpa/co2
hadoop fs -mkdir /data_source_tpa/immatriculations
hdfs dfs -put /vagrant/DataSourceTpa/CO2.csv /data_source_tpa/co2/
hdfs dfs -put /vagrant/DataSourceTpa/Immatriculations.csv /data_source_tpa/immatriculations/

=============

-- Démarrage du serveur Hadoop HIVE
nohup hive --service metastore > /dev/null &
nohup hiveserver2 > /dev/null &

-- Importation des données CSV dans une base de données MongoDB
mongoimport --db tpa --collection clients --type csv --headerline --file /vagrant/DataSourceTpa/Clients_0.csv
mongoimport --db tpa --collection clients --type csv --headerline --file /vagrant/DataSourceTpa/Clients_9.csv

nohup java -Xmx64m -Xms64m -jar $KVHOME/lib/kvstore.jar kvlite -secure-config disable -root $KVROOT &

cd /vagrant

javac -g -cp $KVHOME/lib/kvclient.jar:vagrant/mbds mbds/Tpa.java

java -Xmx256m -Xms256m  -cp $KVHOME/lib/kvclient.jar:/vagrant/ mbds.Tpa

