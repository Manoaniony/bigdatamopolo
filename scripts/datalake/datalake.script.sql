=============

-- Démarrage du serveur Hadoop HIVE
nohup hive --service metastore > /dev/null &
nohup hiveserver2 > /dev/null &

beeline
!connect jdbc:hive2://localhost:10000
	-> oracle
	-> oracle

===============

-- Suppression de la table co2_ext si elle existe
DROP TABLE IF EXISTS co2_ext;

-- Création de la table externe co2_ext pour lire les données stockées en tant que fichier texte dans HDFS.
CREATE EXTERNAL TABLE co2_ext (
    ID INTEGER,
    MARQUE_MODELE STRING,
    BONUS_MALUS STRING,
    REJETS_CO2_G_KM INTEGER,
    COUT_ENERGIE STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE LOCATION 'hdfs:/data_source_tpa/co2'
TBLPROPERTIES ("skip.header.line.count"="1");

-- Sélection des données de la table co2_ext
SELECT * FROM co2_ext LIMIT 20;

==================

-- Suppression de la table immatriculations_ext si elle existe
DROP TABLE IF EXISTS immatriculations_ext;

-- Création de la table externe immatriculations_ext pour lire les données stockées en tant que fichier texte dans HDFS.
Création de la table externe immatriculations_ext pour lire les données stockées en tant que fichier texte dans HDFS.
CREATE EXTERNAL TABLE immatriculations_ext (
    IMMATRICULATION STRING,
    MARQUE STRING,
    NOM STRING,
    PUISSANCE INTEGER,
    LONGUEUR STRING,
    NB_PLACES INTEGER,
    NB_PORTES INTEGER,
    COULEUR STRING,
    OCCASION BOOLEAN,
    PRIX INTEGER
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE 
LOCATION 'hdfs:/data_source_tpa/immatriculations'
TBLPROPERTIES ("skip.header.line.count"="1");

-- Sélection des données de la table immatriculations_ext
SELECT * FROM immatriculations_ext LIMIT 20;

================================

-- Suppression de la table clients_ext si elle existe
DROP TABLE IF EXISTS clients_ext;

-- Création de la table externe clients_ext connectée à MongoDB
CREATE EXTERNAL TABLE clients_ext (
  ID STRING, 
  AGE INTEGER, 
  SEXE STRING,
  TAUX INTEGER,
  SITUATION_FAMILIALE STRING,
  NB_ENFANTS_ACHARGE INTEGER,
  2_EME_VOITURE STRING,
  IMMATRICULATION STRING
)
STORED BY 'com.mongodb.hadoop.hive.MongoStorageHandler'
WITH SERDEPROPERTIES (
  'mongo.columns.mapping'='{"ID":"_id","AGE":"age","SEXE":"sexe","TAUX":"taux","SITUATION_FAMILIALE":"situationFamiliale","NB_ENFANTS_ACHARGE":"nbEnfantsAcharge","2_EME_VOITURE":"2eme voiture","IMMATRICULATION":"immatriculation"}'
)
TBLPROPERTIES (
    'mongo.uri'='mongodb://localhost:27017/tpa.clients'
);

-- Sélection des données de la table clients_ext
SELECT * FROM clients_ext LIMIT 20;

=============================================


=============================================

nohup java -Xmx64m -Xms64m -jar $KVHOME/lib/kvstore.jar kvlite -secure-config disable -root $KVROOT &

cd /vagrant

javac -g -cp $KVHOME/lib/kvclient.jar:vagrant/mbds mbds/Tpa.java

java -Xmx256m -Xms256m  -cp $KVHOME/lib/kvclient.jar:/vagrant/ mbds.Tpa

-- Suppression de la table catalogue_ext si elle existe
DROP TABLE IF EXISTS catalogue_ext;

-- Création de la table externe catalogue_ext
CREATE EXTERNAL TABLE IF NOT EXISTS catalogue_ext (
    catalogueid INTEGER,
    marque STRING,
    nom STRING,
    puissance INTEGER,
    longueur STRING,
    nb_places INTEGER,
    nb_portes INTEGER,
    couleur STRING,
    occasion BOOLEAN,
    prix DOUBLE
)
STORED BY 'oracle.kv.hadoop.hive.table.TableStorageHandler'
TBLPROPERTIES (
    "oracle.kv.kvstore" = "kvstore",
    "oracle.kv.hosts" = "localhost:5000",
    "oracle.kv.tableName" = "CATALOGUE"
);

-- Sélection des données de la table catalogue_ext
SELECT * FROM catalogue_ext LIMIT 20;

=======================================


=======================================

-- Suppression de la table marketing_ext si elle existe
DROP TABLE IF EXISTS marketing_ext;

-- Création de la table externe marketing_ext
CREATE EXTERNAL TABLE IF NOT EXISTS marketing_ext (
    marketingid INTEGER,
    age INTEGER,
    sexe STRING,
    taux INTEGER,
    situation_familiale STRING,
    nb_enfants_acharge INTEGER,
    deuxieme_voiture BOOLEAN
)
STORED BY 'oracle.kv.hadoop.hive.table.TableStorageHandler'
TBLPROPERTIES (
    "oracle.kv.kvstore" = "kvstore",
    "oracle.kv.hosts" = "localhost:5000",
    "oracle.kv.tableName" = "MARKETING"
);

-- Sélection des données de la table marketing_ext
SELECT * FROM marketing_ext LIMIT 20;

-- MAP REDUCE 
    javac CO2*.java
    mkdir org/mbds/tpa
    mv CO2*.class org/mbds/tpa
    jar -cvf co2.jar -C . org
    hadoop fs -rm -r /result
    hadoop jar co2.jar org.mbds.tpa.CO2Driver /data_source_tpa/co2/CO2.csv /result
    hadoop fs -mkdir /result/co2
    hadoop fs -mv /result/part-r-00000 /result/co2

-- Suppression de la table co2_ext si elle existe
    DROP TABLE IF EXISTS co2_ext;

-- Création de la table externe co2_ext pour lire les données stockées en tant que fichier texte dans HDFS.
    CREATE EXTERNAL TABLE co2_ext (
        MARQUE_MODELE STRING,
        BONUS_MALUS INTEGER,
        REJETS_CO2_G_KM INTEGER,
        COUT_ENERGIE INTEGER
    )
    ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
    STORED AS TEXTFILE LOCATION 'hdfs:/result/co2'
    TBLPROPERTIES ("skip.header.line.count"="1");

    SELECT * FROM co2_ext LIMIT 20;

    DROP TABLE IF EXISTS catalogue_co2;

    CREATE TABLE catalogue_co2 AS
    SELECT 
        c.catalogueid, 
        c.marque, 
        c.nom, 
        c.puissance, 
        c.longueur, 
        c.nb_places, 
        c.nb_portes, 
        c.couleur, 
        c.occasion, 
        c.prix, 
        co2.bonus_malus, 
        co2.rejets_co2_g_km, 
        co2.cout_energie
    FROM 
        catalogue_ext c
    LEFT JOIN 
        co2_ext co2
    ON 
        UPPER(c.marque) = UPPER(co2.marque_modele);

    SELECT marque FROM catalogue_co2 c WHERE c.bonus_malus is not null ;

    CREATE TABLE catalogue_co2_updated AS
    SELECT 
        catalogueid, 
        marque, 
        nom, 
        puissance, 
        longueur, 
        nb_places, 
        nb_portes, 
        couleur, 
        occasion, 
        prix, 
        COALESCE(bonus_malus, (SELECT AVG(bonus_malus) FROM catalogue_co2 WHERE bonus_malus IS NOT NULL)) AS bonus_malus,
        COALESCE(rejets_co2_g_km, (SELECT AVG(rejets_co2_g_km) FROM catalogue_co2 WHERE rejets_co2_g_km IS NOT NULL)) AS rejets_co2_g_km,
        COALESCE(cout_energie, (SELECT AVG(cout_energie) FROM catalogue_co2 WHERE cout_energie IS NOT NULL)) AS cout_energie
    FROM catalogue_co2;
   
   select * from catalogue_co2_updated
