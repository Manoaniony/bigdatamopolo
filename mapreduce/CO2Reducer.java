package org.mbds.tpa;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.io.IntWritable;


import java.io.IOException;
import java.util.HashSet;

public class CO2Reducer extends Reducer<Text, Text, Text, Text> {
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        int count = 0 , sumBonus = 0 , sumEmmission = 0 , sumCost = 0  ;
        String marquemodel = "";
        String[] splited ;
        for (Text word : values) {
            splited = word.toString().split(",") ;
            sumBonus += Integer.parseInt(splited[0]) ;
            sumEmmission += Integer.parseInt(splited[1]) ;
            sumCost += Integer.parseInt(splited[2]) ;
            count++ ;
        }
        context.write(new Text(key),  new Text((sumBonus/count) + "," + (sumEmmission/count) + "," + (sumCost/count)));
    }
}

