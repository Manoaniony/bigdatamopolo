package org.mbds.tpa;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.io.IntWritable;


import java.io.IOException;

public class CO2Mapper extends Mapper<LongWritable, Text, Text, Text> {

    private static final Integer DEFAULT_VALUE = 0;
    private static final String EURO_SYMBOL = "€";
    private static final String SPACE = " ";
    private static final String EMPTY_STRING = "";
    private static final String NON_BREAKING_SPACE = "\\u00A0";

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        if (key.get() == 0) {
            return;
        }

        String[] fields = value.toString().split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);

        if (fields.length > 4) {
            // Integer id = Integer.parseInt(fields[0]);
            String marqueModele = extractMarqueModele(fields[1]);
            Integer bonusMalus = extractBonusMalus(fields[2]);
            Integer rejetsCO2 = fields[3] != null ? Integer.parseInt(fields[3]) : DEFAULT_VALUE;
            Integer coutEnergie = fields[4] != null ? Integer.parseInt(fields[4].replaceAll(NON_BREAKING_SPACE, EMPTY_STRING).split("€")[0]) : DEFAULT_VALUE;

            context.write(new Text(marqueModele), new Text(bonusMalus + "," + rejetsCO2 + "," + coutEnergie));

        }
    }

    private String extractMarqueModele(String marqueModele) {
        marqueModele = marqueModele.split(SPACE)[0];
        if (marqueModele.startsWith("\"")) {
            marqueModele = marqueModele.replaceAll("\"", EMPTY_STRING);
            System.out.println("============ marque model " + marqueModele);
        }
        return marqueModele;
    }

    private Integer extractBonusMalus(String bonusMalus) {
        if (bonusMalus != null && bonusMalus.length() != 1) {
            bonusMalus = bonusMalus.replaceAll(NON_BREAKING_SPACE, EMPTY_STRING);
            bonusMalus = bonusMalus.split(EURO_SYMBOL)[0];
        } else {
            bonusMalus = "0";
        }
        return Integer.parseInt(bonusMalus);
    }
}
